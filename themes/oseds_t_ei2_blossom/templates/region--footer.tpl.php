<?php
/**
 * @file
 * Zen theme's implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see zen_preprocess_region()
 * @see template_process()
 */
?>
<?php if ($content): ?>
  <footer id="footer" class="<?php print $classes; ?>">
    <div id="footer-inner">
      <?php print $content; ?>
		  <?php   
			// Get the institution id settings.php file 
			global $conf;
			//$school_end_id = $conf['school_entity_id'];
		//	$ins_address = views_embed_view('ei2_rel_institution_address','default',$school_end_id);?>
		 <!-- <?php if(!empty($ins_address)): ?>
			<div id="inst-address"><?php print $ins_address; ?></div> -->                    
		  <?php endif; ?>
  </div>
  </footer><!-- region__footer -->
<?php endif; ?>
