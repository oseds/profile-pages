; This file describes the core project requirements for profile_pages Several
; patches against Drupal core and their associated issue numbers have been
; included here for reference.

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.23

; ------- installation profile --------

; Download the Profile pages install profile
projects[profile_pages][type] = profile
projects[profile_pages][download][type] = git
projects[profile_pages][download][url] = https://bitbucket.org/oseds/profile-pages.git
;projects[profile_pages][download][branch] = 7.x-1.3
