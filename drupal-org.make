api = 2
core = 7.x

; Modules
projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.0"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.2"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.3"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.3"

projects[eck][subdir] = "contrib"
projects[eck][version] = "2.0-rc2"

projects[superfish][subdir] = "contrib"
projects[superfish][version] = "1.9"

; profile page
projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.1"

projects[oauthconnector][subdir] = "contrib"
projects[oauthconnector][version] = "1.0-beta2"

projects[connector][subdir] = "contrib"
projects[connector][version] = "1.0-beta2"

projects[http_client][subdir] = "contrib"
projects[http_client][version] = "2.4"

; Amazon s3
projects[amazons3][subdir] = "contrib"
projects[amazons3][version] = "1.0-beta7"

projects[awssdk][subdir] = "contrib"
projects[awssdk][version] = "5.4"

projects[amazons3_cors][subdir] = "contrib"
projects[amazons3_cors][version] = "1.x-dev"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.3+9-dev"

; Themes
projects[zen][version] = "5.1"

;Domain Access
projects[domain][subdir] = "contrib"
projects[domain][version] = "3.x-dev"

projects[domain_menu_block][subdir] = "contrib"
projects[domain_menu_block][version] = "1.0-beta2"

projects[domain_path][subdir] = "contrib"
projects[domain_path][version] = "1.0-beta4"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.3"

; Superfish
projects[superfish][subdir] = "contrib"
projects[superfish][version] = "1.9"

;Slideshow
projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.0"
