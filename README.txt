Profile Pages 1.x for Drupal 7.x
----------------------------
Profile Pages platform.

Getting started
---------------
Profile Pages for 7.x requires several patches to be applied to Drupal core. It
provides a `build-profile-pages.make` file for building a full Drupal distro including core
patches as well as a copy of the `Profile Pages` install profile.

1. Grab the `build-profile-pages.make` file from Profile Pages and run:

        $ drush make build-profile-pages.make [directory]

2. Choose the "Profile Pages" install profile when installing Drupal

