<?php
/**
 * @file
 * oseds_domainaccess_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function oseds_domainaccess_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: access content.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: administer menu.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'menu',
  );

  // Exported permission: administer views.
  $permissions['administer views'] = array(
    'name' => 'administer views',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'views',
  );

  // Exported permission: create ct_contact content.
  $permissions['create ct_contact content'] = array(
    'name' => 'create ct_contact content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create ct_feedback content.
  $permissions['create ct_feedback content'] = array(
    'name' => 'create ct_feedback content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create ct_institution content.
  $permissions['create ct_institution content'] = array(
    'name' => 'create ct_institution content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create ct_news content.
  $permissions['create ct_news content'] = array(
    'name' => 'create ct_news content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create ct_slideshow content.
  $permissions['create ct_slideshow content'] = array(
    'name' => 'create ct_slideshow content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create page content.
  $permissions['create page content'] = array(
    'name' => 'create page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ct_contact content.
  $permissions['delete any ct_contact content'] = array(
    'name' => 'delete any ct_contact content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ct_feedback content.
  $permissions['delete any ct_feedback content'] = array(
    'name' => 'delete any ct_feedback content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ct_institution content.
  $permissions['delete any ct_institution content'] = array(
    'name' => 'delete any ct_institution content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ct_news content.
  $permissions['delete any ct_news content'] = array(
    'name' => 'delete any ct_news content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ct_slideshow content.
  $permissions['delete any ct_slideshow content'] = array(
    'name' => 'delete any ct_slideshow content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any page content.
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ct_contact content.
  $permissions['delete own ct_contact content'] = array(
    'name' => 'delete own ct_contact content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ct_feedback content.
  $permissions['delete own ct_feedback content'] = array(
    'name' => 'delete own ct_feedback content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ct_institution content.
  $permissions['delete own ct_institution content'] = array(
    'name' => 'delete own ct_institution content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ct_news content.
  $permissions['delete own ct_news content'] = array(
    'name' => 'delete own ct_news content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ct_slideshow content.
  $permissions['delete own ct_slideshow content'] = array(
    'name' => 'delete own ct_slideshow content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own page content.
  $permissions['delete own page content'] = array(
    'name' => 'delete own page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ct_contact content.
  $permissions['edit any ct_contact content'] = array(
    'name' => 'edit any ct_contact content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ct_feedback content.
  $permissions['edit any ct_feedback content'] = array(
    'name' => 'edit any ct_feedback content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ct_institution content.
  $permissions['edit any ct_institution content'] = array(
    'name' => 'edit any ct_institution content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ct_news content.
  $permissions['edit any ct_news content'] = array(
    'name' => 'edit any ct_news content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ct_slideshow content.
  $permissions['edit any ct_slideshow content'] = array(
    'name' => 'edit any ct_slideshow content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any page content.
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ct_contact content.
  $permissions['edit own ct_contact content'] = array(
    'name' => 'edit own ct_contact content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ct_feedback content.
  $permissions['edit own ct_feedback content'] = array(
    'name' => 'edit own ct_feedback content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ct_institution content.
  $permissions['edit own ct_institution content'] = array(
    'name' => 'edit own ct_institution content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ct_news content.
  $permissions['edit own ct_news content'] = array(
    'name' => 'edit own ct_news content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ct_slideshow content.
  $permissions['edit own ct_slideshow content'] = array(
    'name' => 'edit own ct_slideshow content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own page content.
  $permissions['edit own page content'] = array(
    'name' => 'edit own page content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: publish to any assigned domain.
  $permissions['publish to any assigned domain'] = array(
    'name' => 'publish to any assigned domain',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'domain',
  );

  // Exported permission: view own unpublished content.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
