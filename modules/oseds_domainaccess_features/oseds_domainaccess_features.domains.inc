<?php
/**
 * @file
 * oseds_domainaccess_features.domains.inc
 */

/**
 * Implements hook_domain_menu_block_default_domain_menu_blocks().
 */
function oseds_domainaccess_features_domain_menu_block_default_domain_menu_blocks() {
$dmbs = array();
  $dmbs['ei2_front_menu'] = array(
  'machine_name' => 'ei2_front_menu',
  'base_name' => 'Front Menu',
  'description' => 'Menu Display Based on Institution',
  'level' => '1',
  'depth' => '2',
  'expanded' => '1',
  'follow' => '0',
  'sort' => '0',
  'auto_create' => '1',
  'auto_delete' => '1',
);

return $dmbs;
}
