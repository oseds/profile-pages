<?php
/**
 * @file
 * oseds_domainaccess_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function oseds_domainaccess_features_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function oseds_domainaccess_features_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function oseds_domainaccess_features_image_default_styles() {
  $styles = array();

  // Exported image style: icon_size.
  $styles['icon_size'] = array(
    'name' => 'icon_size',
    'label' => 'icon_size',
    'effects' => array(
      3 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '40',
          'height' => '40',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: ims_slideshow_sizes.
  $styles['ims_slideshow_sizes'] = array(
    'name' => 'ims_slideshow_sizes',
    'label' => 'IMS Slideshow Sizes',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '640',
          'height' => '305',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: omkaar_slideshow_size.
  $styles['omkaar_slideshow_size'] = array(
    'name' => 'omkaar_slideshow_size',
    'label' => 'Omkaar Slideshow Size',
    'effects' => array(),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function oseds_domainaccess_features_node_info() {
  $items = array(
    'ct_contact' => array(
      'name' => t('Contact'),
      'base' => 'node_content',
      'description' => t('Institution Contact List'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ct_feedback' => array(
      'name' => t('Feedback'),
      'base' => 'node_content',
      'description' => t('Users  give feedback to this content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ct_institution' => array(
      'name' => t('Institution'),
      'base' => 'node_content',
      'description' => t('Institution About Details'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ct_news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('News List'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ct_slideshow' => array(
      'name' => t('Slideshow'),
      'base' => 'node_content',
      'description' => t('Slideshows Details'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
